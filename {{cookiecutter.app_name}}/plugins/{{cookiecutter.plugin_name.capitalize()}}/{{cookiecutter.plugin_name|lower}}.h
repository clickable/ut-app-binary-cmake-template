{% if cookiecutter.open_source_license == 'GNU General Public License v3' %}/*
 * Copyright (C) {% now 'local', '%Y'%}  {{cookiecutter.maintainer_name}}
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

{% endif %}#ifndef {{cookiecutter.plugin_name|upper}}_H
#define {{cookiecutter.plugin_name|upper}}_H

#include <QObject>

class {{cookiecutter.plugin_name.capitalize()}}: public QObject {
    Q_OBJECT

public:
    {{cookiecutter.plugin_name.capitalize()}}();
    ~{{cookiecutter.plugin_name.capitalize()}}() = default;

    Q_INVOKABLE void speak();
};

#endif
